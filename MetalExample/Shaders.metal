//
//  Shaders.metal
//  MetalExample
//
//  Created by sxsasha on 16.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

// File for Metal kernel and shader functions

#include <metal_stdlib>
#include <simd/simd.h>

// Including header shared between this Metal shader code and Swift/C code executing Metal API commands
#import "ShaderTypes.h"

using namespace metal;

typedef struct
{
    float2 position [[attribute(VertexAttributePosition)]];
    float4 color [[attribute(VertexAttributeColor)]];
    float2 texCoord [[attribute(VertexAttributeTexCoord)]];
} Vertex;

typedef struct
{
    float4 position [[position]];
    float4 color;
    float2 texCoord;
} VertexOut;

vertex VertexOut vertexShader(uint vertexID [[ vertex_id ]],
                              device Particle *particles [[ buffer(BufferIndexVertex) ]],
                              Vertex in [[stage_in]],
                              constant Uniforms &uniforms [[ buffer(BufferIndexUniforms) ]],
                              constant float *aspectRatio  [[ buffer(BufferIndexAspectRatio) ]])
{
    VertexOut out;
    
    float2 position = particles[vertexID].position;
    float4 color = particles[vertexID].color;
    float2 texCoord = particles[vertexID].textCoord;
    
    position.x = position.x * float(*aspectRatio);
    
    out.position = float4(position, 0.2, 1.0);// * uniforms.projectionMatrix;
    out.color = color; //in.color;
    out.texCoord = texCoord;
    
    return out;
}

fragment float4 fragmentShader(VertexOut in [[stage_in]],
                               texture2d<half> circleTexture [[ texture(TextureIndexColor) ]])
{
    constexpr sampler colorSampler(mip_filter::linear,
                                   mag_filter::linear,
                                   min_filter::linear);
    
    half4 textureColor = circleTexture.sample(colorSampler, in.texCoord.xy);
    
//    if (textureColor.a < 0.5) {
//        discard_fragment();
//    }
    
    return float4(textureColor * in.color.x); //in.color;
}

//vertex ColorInOut vertexShader(Vertex in [[stage_in]],
//                               constant Uniforms &uniforms [[ buffer(BufferIndexUniforms) ]])
//{
//    ColorInOut out;
//
//    float4 position = float4(in.position, 1.0);
//    out.position = uniforms.projectionMatrix * uniforms.modelViewMatrix * position;
//    out.texCoord = in.texCoord;
//
//    return out;
//}
//
//fragment float4 fragmentShader(ColorInOut in [[stage_in]],
//                               constant Uniforms &uniforms [[ buffer(BufferIndexUniforms) ]],
//                               texture2d<half> colorMap     [[ texture(TextureIndexColor) ]])
//{
//    constexpr sampler colorSampler(mip_filter::linear,
//                                   mag_filter::linear,
//                                   min_filter::linear);
//
//    half4 colorSample = colorMap.sample(colorSampler, in.texCoord.xy);
//
//    return float4(colorSample);
//}

