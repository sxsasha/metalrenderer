//
//  Star.swift
//  MetalExample
//
//  Created by sxsasha on 28.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

import Foundation

struct Star {
    var position: simd_float2 {
        didSet {
            reCountTriangles()
        }
    }
    
    var size: simd_float1 {
        didSet {
            reCountTriangles()
        }
    }
    
    var color: simd_float4
    private var positions = [simd_float2](repeating: simd_float2(), count: 6)
    private let textCoord = [simd_float2(0.0, 0.0), simd_float2(1.0, 0.0), simd_float2(0.0, 1.0),
                             simd_float2(0.0, 1.0), simd_float2(1.0, 0.0), simd_float2(1.0, 1.0)]
    
    private mutating func reCountTriangles() {
        let half = size / 2.0
        let startPoint = simd_float2(position.x - half, position.y + half)
        let endPoint = simd_float2(position.x + half, position.y - half)
        positions[0] = simd_float2(position.x - half, position.y - half)
        positions[1] = endPoint
        positions[2] = startPoint
        
        positions[3] = startPoint
        positions[4] = endPoint
        positions[5] = simd_float2(position.x + half, position.y + half)
    }
    
    init(position: simd_float2, size: simd_float1, color: simd_float4) {
        self.position = position
        self.size = size
        self.color = color
        
        reCountTriangles()
    }
    
    var particles: [Particle] {
        return [Particle(position: positions[0], color: color, textCoord: textCoord[0]),
                Particle(position: positions[1], color: color, textCoord: textCoord[1]),
                Particle(position: positions[2], color: color, textCoord: textCoord[2]),
                Particle(position: positions[3], color: color, textCoord: textCoord[3]),
                Particle(position: positions[4], color: color, textCoord: textCoord[4]),
                Particle(position: positions[5], color: color, textCoord: textCoord[5])]
    }
}
