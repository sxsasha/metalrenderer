//
//  Renderer.swift
//  MetalExample
//
//  Created by sxsasha on 16.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

// Our platform independent renderer class

import Metal
import MetalKit
import simd

class SimpleRenderer: NSObject, Renderer {
    
    // The 256 byte aligned size of our uniform structure
    static let alignedVerticesSize = (MemoryLayout<Particle>.size & ~0xFF) + 0x100
    static let alignedUniformsSize = (MemoryLayout<Uniforms>.size & ~0xFF) + 0x100
    
    static let maxBuffersInFlight = 3
    static let maxStars = 1000
    
    public let device: MTLDevice
    let commandQueue: MTLCommandQueue
    var dynamicVertextBuffer: MTLBuffer
    var dynamicUniformBuffer: MTLBuffer
    var pipelineState: MTLRenderPipelineState
    var depthState: MTLDepthStencilState
    var circleTexture: MTLTexture
    
    // The current size of our view so we can use this in our render pipeline
    var projectionMatrix: matrix_float4x4 = matrix_float4x4(diagonal: float4(1.0))
    var aspectRatio: simd_float1 = simd_float1()
    
    let inFlightSemaphore = DispatchSemaphore(value: SimpleRenderer.maxBuffersInFlight)
    
    var particles: UnsafeMutablePointer<Particle>
    var uniforms: UnsafeMutablePointer<Uniforms>
    
    var stars = [Star]()
    
    //=========================================================
    // MARK: - Initialization & Config & Load
    //=========================================================
    required init?(metalKitView: MTKView) {
        /// save device
        self.device = metalKitView.device!
        /// create commandQueue
        guard let queue = self.device.makeCommandQueue() else {
            return nil
        }
        self.commandQueue = queue
        
        /// init dynamicVertextBuffer
        let verticesBufferSize = SimpleRenderer.alignedVerticesSize * 6 * SimpleRenderer.maxStars
        guard let vertextBuffer = self.device.makeBuffer(length: verticesBufferSize,
                                                         options: [.storageModeShared]) else {
            return nil
        }
        dynamicVertextBuffer = vertextBuffer
        particles = vertextBuffer.contents().bindMemory(to: Particle.self, capacity: SimpleRenderer.maxStars * 6)

        /// init dynamicUniformBuffer
        let uniformBufferSize = SimpleRenderer.alignedUniformsSize
        guard let uniformBuffer = self.device.makeBuffer(length: uniformBufferSize, options: [.storageModeShared]) else {
            return nil
        }
        
        dynamicUniformBuffer = uniformBuffer
        self.dynamicUniformBuffer.label = "UniformBuffer"
        uniforms = dynamicUniformBuffer.contents().bindMemory(to:Uniforms.self, capacity:1)
        
        /// config metalKitView
        metalKitView.depthStencilPixelFormat = MTLPixelFormat.depth32Float_stencil8
        metalKitView.colorPixelFormat = MTLPixelFormat.bgra8Unorm_srgb
        metalKitView.sampleCount = 1
        
        /// create mtlVertexDescriptor for pipeline & mesh
        let mtlVertexDescriptor = SimpleRenderer.buildMetalVertexDescriptor()
        
        /// build render pipeline
        do {
            pipelineState = try SimpleRenderer.buildRenderPipelineWithDevice(device: device,
                                                                       metalKitView: metalKitView,
                                                                       mtlVertexDescriptor: mtlVertexDescriptor)
        } catch {
            print("Unable to compile render pipeline state.  Error info: \(error)")
            return nil
        }
        
        /// depthState
        let depthStateDesciptor = MTLDepthStencilDescriptor()
        depthStateDesciptor.depthCompareFunction = MTLCompareFunction.lessEqual
        depthStateDesciptor.isDepthWriteEnabled = true
        guard let state = device.makeDepthStencilState(descriptor:depthStateDesciptor) else {
            return nil
        }
        
        depthState = state
        
        /// texture
        do {
            circleTexture = try ComplexRenderer.loadTexture(device: device, textureName: "circle")
        } catch {
            print("Unable to load texture. Error info: \(error)")
            return nil
        }

        super.init()
        
        createParticles()
    }
    
    class func buildMetalVertexDescriptor() -> MTLVertexDescriptor {
        // Creete a Metal vertex descriptor specifying how vertices will by laid out for input into our render
        //   pipeline and how we'll layout our Model IO vertices
        
        let mtlVertexDescriptor = MTLVertexDescriptor()

        //positions
        mtlVertexDescriptor.attributes[VertexAttribute.position.rawValue].format = MTLVertexFormat.float2
        mtlVertexDescriptor.attributes[VertexAttribute.position.rawValue].offset = 0
        mtlVertexDescriptor.attributes[VertexAttribute.position.rawValue].bufferIndex = BufferIndex.vertex.rawValue
        
        //color
        mtlVertexDescriptor.attributes[VertexAttribute.color.rawValue].format = MTLVertexFormat.float4
        mtlVertexDescriptor.attributes[VertexAttribute.color.rawValue].offset = 16
        mtlVertexDescriptor.attributes[VertexAttribute.color.rawValue].bufferIndex = BufferIndex.vertex.rawValue
        
        //textureCoord
        mtlVertexDescriptor.attributes[VertexAttribute.texCoord.rawValue].format = MTLVertexFormat.float2
        mtlVertexDescriptor.attributes[VertexAttribute.texCoord.rawValue].offset = 32
        mtlVertexDescriptor.attributes[VertexAttribute.texCoord.rawValue].bufferIndex = BufferIndex.vertex.rawValue
        
        mtlVertexDescriptor.layouts[BufferIndex.vertex.rawValue].stride = 48
        mtlVertexDescriptor.layouts[BufferIndex.vertex.rawValue].stepRate = 1
        mtlVertexDescriptor.layouts[BufferIndex.vertex.rawValue].stepFunction = MTLVertexStepFunction.perVertex
        
        return mtlVertexDescriptor
    }
    
    class func buildRenderPipelineWithDevice(device: MTLDevice,
                                             metalKitView: MTKView,
                                             mtlVertexDescriptor: MTLVertexDescriptor) throws -> MTLRenderPipelineState {
        /// Build a render state pipeline object
        
        let library = device.makeDefaultLibrary()
        
        let vertexFunction = library?.makeFunction(name: "vertexShader")
        let fragmentFunction = library?.makeFunction(name: "fragmentShader")
        
        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.label = "RenderPipeline"
        pipelineDescriptor.sampleCount = metalKitView.sampleCount
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.vertexDescriptor = mtlVertexDescriptor
        
        pipelineDescriptor.colorAttachments[0].pixelFormat = metalKitView.colorPixelFormat
        pipelineDescriptor.colorAttachments[0].isBlendingEnabled = true
        
        pipelineDescriptor.colorAttachments[0].rgbBlendOperation = .add
        pipelineDescriptor.colorAttachments[0].alphaBlendOperation = .add
        
        pipelineDescriptor.colorAttachments[0].sourceRGBBlendFactor = .sourceAlpha
        pipelineDescriptor.colorAttachments[0].sourceAlphaBlendFactor = .sourceAlpha
        
        pipelineDescriptor.colorAttachments[0].destinationRGBBlendFactor = .oneMinusSourceAlpha
        pipelineDescriptor.colorAttachments[0].destinationAlphaBlendFactor = .oneMinusSourceAlpha
        
        pipelineDescriptor.depthAttachmentPixelFormat = metalKitView.depthStencilPixelFormat
        pipelineDescriptor.stencilAttachmentPixelFormat = metalKitView.depthStencilPixelFormat
        
        return try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
    }
    
    class func loadTexture(device: MTLDevice,
                           textureName: String) throws -> MTLTexture {
        /// Load texture data with optimal parameters for sampling
        
        let textureLoader = MTKTextureLoader(device: device)
        
        let textureLoaderOptions = [
            MTKTextureLoader.Option.textureUsage: NSNumber(value: MTLTextureUsage.shaderRead.rawValue),
            MTKTextureLoader.Option.textureStorageMode: NSNumber(value: MTLStorageMode.`private`.rawValue)
        ]
        
        return try textureLoader.newTexture(name: textureName,
                                            scaleFactor: 1.0,
                                            bundle: nil,
                                            options: textureLoaderOptions)
        
    }
    
    //=========================================================
    // MARK: - Update
    //=========================================================
    private func createParticles() {
        stars.removeAll()
        var vertexData = Array<Particle>()
        for _ in 0..<SimpleRenderer.maxStars {
            let x = (Float(arc4random() % 200) / 100) - 1.0
            let y = (Float(arc4random() % 200) / 100) - 1.0
            let size: Float = Float(arc4random() % 11) / 1000
            let alpha: Float = Float(arc4random() % 100) / 100

            let star = Star(position: simd_float2(x, y), size: size, color: simd_float4(alpha, alpha, alpha, alpha))
            stars.append(star)

            vertexData += star.particles
        }

        particles.initialize(from: UnsafePointer(vertexData), count: SimpleRenderer.maxStars * 6)
        //particles.assign(from: UnsafePointer(vertexData), count: SimpleRenderer.maxParticles * 6)
    }
    
    private func updateParticles() {
        var vertexData = Array<Particle>()

        for (i, star) in stars.enumerated() {
            let alpha = star.color.w * 360.0
            let sinNextValue = sin((alpha * Float.pi) / 180.0)
            
            let normilize = (sinNextValue + 1.0) / 2.0
            
            stars[i].color = simd_float4(normilize, normilize, normilize, (alpha + 1) / 360.0)
            
            vertexData += star.particles
        }

        particles.assign(from: UnsafePointer(vertexData), count: SimpleRenderer.maxStars * 6)
        //particles.moveAssign(from: UnsafeMutablePointer(mutating: vertexData), count: SimpleRenderer.maxParticles * 6)
        //particles.initialize(from: UnsafePointer(vertexData), count: SimpleRenderer.maxParticles * 6)
    }

    /// Per frame updates hare
    func draw(in view: MTKView) {
        guard let commandBuffer = commandQueue.makeCommandBuffer() else {
            return
        }
        
        _ = inFlightSemaphore.wait(timeout: DispatchTime.distantFuture)
        
        let semaphore = inFlightSemaphore
        commandBuffer.addCompletedHandler { (_ commandBuffer)-> Swift.Void in
            semaphore.signal()
        }

        updateParticles()
        
        /// Delay getting the currentRenderPassDescriptor until we absolutely need it to avoid
        ///   holding onto the drawable and blocking the display pipeline any longer than necessary
        
        guard let renderPassDescriptor = view.currentRenderPassDescriptor else {
            return
        }
        
        renderPassDescriptor.colorAttachments[0].loadAction = .clear
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, 1.0)
        
        guard let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else {
                commandBuffer.commit()
                return
        }

        /// Final pass rendering code here
        renderEncoder.label = "Primary Render Encoder"
        
        renderEncoder.pushDebugGroup("Draw Box")
        
        renderEncoder.setCullMode(.back)
        
        renderEncoder.setFrontFacing(.counterClockwise)
        
        renderEncoder.setRenderPipelineState(pipelineState)
        
        renderEncoder.setDepthStencilState(depthState)

        renderEncoder.setVertexBuffer(dynamicVertextBuffer, offset:0, index: BufferIndex.vertex.rawValue)
        
        renderEncoder.setVertexBuffer(dynamicUniformBuffer, offset:0, index: BufferIndex.uniforms.rawValue)
        
        renderEncoder.setVertexBytes(&aspectRatio,
                                     length: MemoryLayout.size(ofValue: aspectRatio),
                                     index: BufferIndex.aspectRatio.rawValue)
        
        renderEncoder.setFragmentTexture(circleTexture, index: TextureIndex.color.rawValue)
        
        renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: SimpleRenderer.maxStars * 6)
//        renderEncoder.drawPrimitives(type: .point,
//                                     vertexStart: 0,
//                                     vertexCount: SimpleRenderer.maxParticles)
        
        renderEncoder.popDebugGroup()
        
        renderEncoder.endEncoding()
        
        if let drawable = view.currentDrawable {
            commandBuffer.present(drawable)
        }
        
        commandBuffer.commit()
    }
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        /// Respond to drawable size or orientation changes here
        
        aspectRatio = simd_float1(size.height / size.width)
    }
}

extension SimpleRenderer {
    func refreshDrawing() {
        createParticles()
    }
}
