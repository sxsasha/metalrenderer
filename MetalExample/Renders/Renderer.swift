//
//  Renderer.swift
//  MetalExample
//
//  Created by sxsasha on 22.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

import MetalKit

enum RendererError: Error {
    case badVertexDescriptor
}

protocol Renderer: MTKViewDelegate {
    init?(metalKitView: MTKView)
    func refreshDrawing()
}
