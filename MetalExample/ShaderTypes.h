//
//  ShaderTypes.h
//  MetalExample
//
//  Created by sxsasha on 16.03.18.
//  Copyright © 2018 sxsasha. All rights reserved.
//

//
//  Header containing types and enum constants shared between Metal shaders and Swift/ObjC source
//
#ifndef ShaderTypes_h
#define ShaderTypes_h

#ifdef __METAL_VERSION__
#define NS_ENUM(_type, _name) enum _name : _type _name; enum _name : _type
#define NSInteger metal::int32_t
#else
#import <Foundation/Foundation.h>
#endif

#include <simd/simd.h>

typedef NS_ENUM(NSInteger, BufferIndex)
{
    BufferIndexMeshPositions = 0,
    BufferIndexMeshGenerics  = 1,
    BufferIndexUniforms      = 2,
    BufferIndexVertex        = 3,
    BufferIndexAspectRatio   = 4
};

typedef NS_ENUM(NSInteger, VertexAttribute)
{
    VertexAttributePosition = 0,
    VertexAttributeColor  = 1,
    VertexAttributeTexCoord  = 2
};

typedef NS_ENUM(NSInteger, TextureIndex)
{
    TextureIndexColor    = 0,
};

typedef struct
{
    matrix_float4x4 projectionMatrix;
    matrix_float4x4 modelViewMatrix;
} Uniforms;

typedef struct
{
    vector_float2 position;
    vector_float4 color;
    vector_float2 texCoord;
} Triangle;

typedef struct
{
    float size;
} UniformSize;

typedef struct
{
    vector_float2 position;
    vector_float4 color;
    vector_float2 textCoord;
} Particle;

#endif /* ShaderTypes_h */

